# Object Detecttion

In order to use these demos, you will need the following:

 - A [`model.tflite`](https://storage.googleapis.com/export_train_data/draw_card/train_2019-11-07/model.tflite)
 - A [`labelmap.txt`](https://storage.googleapis.com/export_train_data/draw_card/train_2019-11-07/labelmap.txt)
 - Demo data, in the form of one or multiple images, a video file, or a webcam.

You may also opt to download [model metadata](https://storage.googleapis.com/export_train_data/draw_card/train_2019-11-07/metadata.json) which contains information about the trained model if you wish to implement something on another platform (i.e. Android), however it is not required for these examples.

If you choose to run these demos on the Raspberry Pi, you will need to install either the "large" Tensorflow package or one of these smaller wheels
 - [Python 3.5](https://dl.google.com/coral/python/tflite_runtime-1.14.0-cp35-cp35m-linux_armv7l.whl)
 - [Python 3.7](https://dl.google.com/coral/python/tflite_runtime-1.14.0-cp37-cp37m-linux_armv7l.whl)

Note that the Raspberry Pi 4 will need one of these images if you choose to use the 64-bit version of Raspbian.
 - [Python 3.5](https://dl.google.com/coral/python/tflite_runtime-1.14.0-cp35-cp35m-linux_aarch64.whl)
 - [Python 3.7](https://dl.google.com/coral/python/tflite_runtime-1.14.0-cp37-cp37m-linux_aarch64.whl)